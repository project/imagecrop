Description
-----------
This is a development version, do not use it on production site unless you know what
you are doing.

This module makes a javascript toolbox action available thanks to the power
of Imagecache 2. It can currently 'hook' into the image module and cck imagefield widget.
It will add a 'javascript crop' link on the edit form of an image node or a node with an
imagefield widget. The popup window will display all available imagecache presets with
a javascript crop action. In your theming you can use the imagecache theme function with
a preset. The imagecache action will make a database call to choose the right crop area.

The main difference with projects like eyedrop or imagefield_crop is that it doesn't 
provide it's own  widget to upload images, instead it just 'hooks' into image files.
In the future, more modules will be supported and eventually a 'image file browser' 
will also be made available.

Everyone is invited to submit patches for more module support. I might
even give some people cvs access.

Installation
------------
You need imagecache 2, imageapi and jquery_interface.
You need to apply the patch to the imagecache module. (patch -p0 < imagecache.patch) 
(Support this module by going to http://drupal.org/node/231663 and review the patch)
After you enable the module, you can go to admin/settings/imagecrop and enable 
support for modules & fields to display a link.

Copy the page-imagecrop.tpl.php to your /themes/yourtheme directory. This way, only
needed html will be displayed for the cropping (there are still some css-issues though,
but it will work for now).

A cron task cleans up the imagecrop table clearing records from files and/or presets
which do not exist anymore, so make sure cron is running.

Features, support, bugs etc
---------------------------
File request,bugs,patches on http://drupal.org/project/imagecrop

Inspiration
-----------
Came from the imagefield_crop module on which I based the html and jquery 
with some adjustments.

Todo
----
Major code improvements like javascript,css & language cleanup, help descriptions, 
support for modules and more. cf @todo's in all files.
6.x port (depends on imagecache2 for D6)

Author
------
Kristof De Jaeger - http://drupal.org/user/107403 - http://realize.be
