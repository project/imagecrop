<?php

/**
 * Form to add values for javascript form
 * 
 * @param $data values passed on by imagecache
 */

function imagecrop_javascript_form($data) {
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => $data['width'],
    '#description' => t('Enter a width in pixels or as a percentage. i.e. 500 or 80%.'),
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => $data['height'],
    '#description' => t('Enter a height in pixels or as a percentage. i.e. 500 or 80%.'),
  );
  $form['xoffset'] = array(
    '#type' => 'textfield',
    '#title' => t('X offset'),
    '#default_value' => $data['xoffset'],
    '#description' => t('Enter an offset in pixels or use a keyword: <em>left</em>, <em>center</em>, or <em>right</em>.'),
  );
  $form['yoffset'] = array(
    '#type' => 'textfield',
    '#title' => t('Y offset'),
    '#default_value' => $data['yoffset'],
    '#description' => t('Enter an offset in pixels or use a keyword: <em>top</em>, <em>center</em>, or <em>bottom</em>.'),
  );
  $form['resizable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is the toolbox resizable or not?'),
    '#default_value' => $data['resizable'],
  );
  return $form;
}

/**
 * Display properties of a single action
 *
 * @param $element passed on by imagecache
 * @return string 
 */
function theme_imagecrop_javascript($element) {
  $data = $element['#value'];
  return 'width: '. $data['width'] .', height: '. $data['height'] .', xoffset: '. $data['xoffset'] .', yoffset: '. $data['yoffset'] .', resizable: '.$data['resizable'];
}

/**
 * Callback todo the javascript crop action on an image
 *
 * @param $image current image resource
 * @param $data values associated with this action
 * @param $presetid id of preset
 * @return false or true
 */
function imagecrop_javascript_image(&$image, $data, $presetid) {
  $row = db_fetch_object(db_query("SELECT xoffset,yoffset,width,height FROM {imagecrop} ic INNER JOIN {files} f on f.fid = ic.fid WHERE f.filepath = '%s' AND ic.presetid = %d",$image->source,$presetid));
  if (!empty($row)) {
    $data['xoffset'] = $row->xoffset;
    $data['yoffset'] = $row->yoffset;
    $data['width'] = $row->width;
    $data['height'] = $row->height;
  }
  if (!imageapi_image_crop($image, $data['xoffset'], $data['yoffset'], $data['width'], $data['height'])) {
      watchdog('imagecrop', t('imagecrop_javascript failed. image: %image, data: %data.', array('%path' => $image, '%data' => print_r($data, TRUE))), WATCHDOG_ERROR);
    return FALSE;
  }
  return TRUE;
}
